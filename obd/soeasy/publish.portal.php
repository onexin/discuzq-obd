<?php

/**
 * ONEXIN BIG DATA For Other 1.4+
 * ============================================================================
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * @package    onexin_bigdata
 * @module     portal
 * @date       2021-04-12
 * @author     King
 * @copyright  Copyright (c) 2021 Onexin Platform Inc. (http://www.onexin.com)
 */

if (!defined('OBD_CONTENT')) {
    exit('Access Denied');
}

/*
//--------------Tall us what you think!----------------------------------
*/

ignore_user_abort();

//-----------------------------VEST--------------------------------------------

$vest = addslashes(bigdata_randone($_OBD['portal_users']));
$member = DB::fetch_first("SELECT id,username FROM " . DB::table('users') . " WHERE `username` = '$vest' LIMIT 1");
$userid = !empty($member['id']) ? $member['id'] : 1;

//-----------------------------FROM URL/SITENAME--------------------------------------------

        $_OBD['from_style2'] = str_replace(
            array('{occurl}', '{occsite}', '{occtitle}'),
            array($_POST['occurl'], $_POST['occsite'], $_POST['title']),
            $_OBD['from_style2']
        );
        $_POST['content'] = str_replace('{OCC}', $_OBD['from_style2'], $_POST['content']);

//-----------------------------READY GO--------------------------------------------

        // video
        //$_POST['content'] = preg_replace("/\[video\](.*?)\[\/video\]/", '\\1', $_POST['content']);

        // <!--more-->
        $_POST['content'] = preg_replace("/\<hr\>$/", '', $_POST['content']);

        // dzq
        $_POST['content'] = _dzqcode($_POST['content']);

        // views
        $views = '1';
        if ($_OBD['origviews']) {
            if (preg_match("/^(\d+).*?(\d+)$/", $_OBD['origviews'], $match)) {
                $origviews = rand($match[1], $match[2]);
                $views = intval($origviews);
            }
        }

//---------------POST thread-------------------------------------------------------
        $catid = explode('|', $_POST['catid']);
        //cate_id
        $cate_id = intval($catid[0]);
        // Q3.0 类型：0普通 1长文 2视频 3图片 99全部
        $type_id = 99;
        $description = _summary($_POST['content'], 200);
        $title          = addslashes(trim($_POST['title']));
        $content        = addslashes(trim($_POST['content']));
        $addtime        = date("Y-m-d H:i:s", time());
        //是否显示
        $is_display     = 1;

        // thread
        $sql = "INSERT INTO " . DB::table('threads') . " (title,category_id,type,user_id,last_posted_user_id,created_at,updated_at,view_count,is_display,post_count) VALUES ('$title','$cate_id','$type_id','$userid','$userid','$addtime','$addtime','$views','$is_display','1')";
        $db->query($sql);
        $art_id = $db->insert_id();

        // posts
        $sql = "INSERT INTO " . DB::table('posts') . " (thread_id,user_id,content,created_at,updated_at,is_first) VALUES ('$art_id','$userid','$content','$addtime','$addtime','1')";
        $db->query($sql);
        if ($art_id) {
            DB::query("UPDATE " . DB::table('plugin_onexin_bigdata') . " SET `status` = '1', `ip` = 'portal|" . $art_id . "' WHERE `k` = '$_POST[k]'");
        }

//---------------End post new article-------------------------------------------------------

bigdata_output("200", array("id" => $art_id));
exit;

//-----------------------------End--------------------------------------------

function _dzqcode($html)
{

    return $html;
    //$html = preg_replace("/<img src=\"(http.*?)\" border=\"0\">/", '<IMG alt="DESC0001.jpg" src="\\1" title=" ">![DESC0001.jpg](\\1 " ")</IMG>', $html);
    //return '<r>' . $html . '</r>';
}

// b5038123-a4d3-437e-b21c-9f6819af1fb9
function _uuid()
{

    $charid = strtolower(md5(uniqid(rand(), true)));
    return substr($charid, 0, 8) . '-a4d3-437e-b21c-9f6819af1fb9';
}

function _summary($message, $length = 200)
{

    $message = str_replace('　', '', stripslashes($message));
    $message = preg_replace(array("/\[video\].*?\[\/video\]/", "/\[attach\].*?\[\/attach\]/", "/\&[a-z]+\;/i", "/\<script.*?\<\/script\>/"), '', $message);
    $message = preg_replace("/\[.*?\]/", '', $message);
    $message = _cutstr(strip_tags($message), $length);
    return $message;
}

function _cutstr($string, $length, $dot = ' ...')
{

    if (strlen($string) <= $length) {
        return $string;
    }

    $string = htmlspecialchars_decode($string);
//  if (function_exists('mb_substr')){
        $strcut = mb_substr($string, 0, $length, 'utf-8');
//    }elseif (function_exists('iconv_substr')){
//       $strcut = iconv_substr($string, 0, $length, 'utf-8');
//    }else{
//      $strcut = substr($string, 0, $length);
//   }

    $strcut = htmlspecialchars($strcut);
    $strcut = str_replace(array('\''), array('&#39;'), $strcut);
    if ($dot && $string != $strcut) {
        $strcut .= '...';
    }

    return $strcut . $dot;
}
