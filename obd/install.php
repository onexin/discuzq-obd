<?php

/**
 * ONEXIN BIG DATA For Other 5.5+
 * ============================================================================
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * @package    onexin_bigdata
 * @module     api
 * @date       2021-06-29
 * @author     King
 * @copyright  Copyright (c) 2021 Onexin Platform Inc. (http://www.onexin.com)
 */

/*
//--------------Tall us what you think!----------------------------------
*/

    include_once __DIR__ . '/load.other.php';
$sql = <<<EOF

CREATE TABLE IF NOT EXISTS `pre_plugin_onexin_bigdata` (
  `bid` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url` text NOT NULL,
  `k` varchar(32) NOT NULL DEFAULT '',
  `catid` varchar(20) NOT NULL DEFAULT '',
  `i` varchar(32) NOT NULL DEFAULT '',
  `resid` varchar(20) NOT NULL DEFAULT '',
  `dateline` int(10) unsigned NOT NULL DEFAULT '0',
  `cronpublishdate` int(10) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(20) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`bid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

EOF;
$sql = str_replace(array(' `pre_', ' CHARSET=utf8'), array(' `' . $table, ' CHARSET=' . $dbcharset), $sql);
$db->query($sql);

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>安装(Install)</title>
</head>

<body>
<p>安装成功了，请删除该文件。 </p>
<p>The installation was successful, please delete this file.</p>
</body>
</html>
<?php
exit;
